/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercice1;

import java.rmi.*;

/**
 *
 * @author agiavari
 */
public interface Compute extends Remote {
    // Contient les methodes qui seront appelées à distance via les objets
    public int multiply(int a, int b)throws RemoteException;   
}
