/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercice1;

import java.net.InetAddress;
import java.rmi.Naming;

/**
 *
 * @author agiavari
 */
public class Client {
    public static void main(String args []){
        //Il faut modifier la politique de securité pour pouvoir connecter le client
        System.setProperty("java.security.policy","C:\\Users\\agiavari\\Documents\\NetBeansProjects\\rmi\\src\\Exercice1\\java.policy");
        
        //Security manager
        if(System.getSecurityManager() == null){
            System.setSecurityManager(new SecurityManager());
        }
        
        try {
            //Trouver l'objet distant grâce a son nom/adresse
            Compute cp= (Compute)Naming.lookup("rmi://"+InetAddress.getLocalHost().getHostAddress()+"/ComputeMul");
            System.out.println(cp.multiply(10,5));
        }catch(Exception e){System.out.println(e);}
            
    }
}
