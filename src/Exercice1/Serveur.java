/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercice1;

import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 *
 * @author agiavari
 */
public class Serveur {

    public static void main(String args []) {
        try {
            System.out.println("Serveur ouvert");
            LocateRegistry.createRegistry(1099);//Registre à l'adresse voulue
            
            //Nom qui sera enregistré dans le registre est utilisé par le client
            //Pour trouver l'objet
            String name = "rmi://"+InetAddress.getLocalHost().getHostAddress()+"/ComputeMul";
            //On créait l'objet distant
            ComputeMul cpMul = new ComputeMul();
            
            Naming.rebind(name,cpMul); //Enregistre l'objet 
            
        }catch(Exception e){System.out.println(e);}     
    }
}
