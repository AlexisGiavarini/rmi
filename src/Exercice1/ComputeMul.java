/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercice1;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author agiavari
 */
public class ComputeMul extends UnicastRemoteObject implements Compute{
 
    //Implementation de l'interface Compute
    //Il s'agit de l'objet distant qui executera les fonctions appelées
   
    ComputeMul() throws RemoteException { // Pour forcer la levée d'exception
        super();
    } 
    
    @Override
    public int multiply(int a, int b) {
        return a * b;
    }  
}
