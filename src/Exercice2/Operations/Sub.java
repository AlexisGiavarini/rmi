/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercice2.Operations;

import Exercice2.Task;
import java.math.BigInteger;


/**
 *
 * @author agiavari
 */
public class Sub implements Task {
    BigInteger val1,val2,res;
    
    public Sub(BigInteger pVal1,BigInteger pVal2){
        this.val1 = pVal1;
        this.val2 = pVal2;
    }
    
    public Object execute(){
        return computeSub();
    }
    
    public BigInteger computeSub(){
        return this.val1.subtract(this.val2);
    }
}
