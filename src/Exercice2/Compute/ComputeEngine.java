/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercice2.Compute;
import Exercice2.Compute.Compute;
import Exercice2.Task;
import java.net.InetAddress;
import java.rmi.*;
import java.rmi.server.*;
/**
 *
 * @author agiavari
 */
public class ComputeEngine extends UnicastRemoteObject implements Compute {
    public ComputeEngine() throws RemoteException{}
    
    @Override
    public Object executeTask(Task t) throws RemoteException {
        return t.execute();
    } 
}
