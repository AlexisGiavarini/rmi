/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercice2.Compute;

import Exercice2.Task;
import java.rmi.*;

/**
 *
 * @author agiavari
 */
public interface Compute extends Remote {
    
    Object executeTask(Task t) throws RemoteException;
}
