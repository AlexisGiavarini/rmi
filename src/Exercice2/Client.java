/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercice2;

import Exercice2.Operations.Add;
import Exercice2.Compute.Compute;
import Exercice2.Operations.Div;
import Exercice2.Operations.Mul;
import Exercice2.Operations.Sub;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 *
 * @author agiavari
 */
public class Client {
    public static void main(String args []) throws UnknownHostException, NotBoundException, MalformedURLException, RemoteException{
        //Il faut modifier la politique de securité pour pouvoir connecter le client
        System.setProperty("java.security.policy","C:\\Users\\agiavari\\Documents\\NetBeansProjects\\rmi\\src\\Exercice2\\java.policy");
        
        //Security manager
        if(System.getSecurityManager() == null){
            System.setSecurityManager(new SecurityManager());
        }
        
        Compute cp= (Compute)Naming.lookup("rmi://"+InetAddress.getLocalHost().getHostAddress()+"/Compute");
        
        BigInteger a = BigInteger.valueOf(10);
        BigInteger b = BigInteger.valueOf(90);
        
        Add addTask = new Add(a,b);
        System.out.println("Addition de "+a+" et "+b+" = "+cp.executeTask(addTask));
        
        Sub subTask = new Sub(b,a);
        System.out.println("Soustraction de "+b+" et "+a+" = "+cp.executeTask(subTask));
        
        Mul mulTask = new Mul(a,b);
        System.out.println("Multiplication de "+a+" et "+b+" = "+cp.executeTask(mulTask));
        
        Div divTask = new Div(b,a);
        System.out.println("Division de "+b+" et "+a+" = "+cp.executeTask(divTask));
                  
    }
}
