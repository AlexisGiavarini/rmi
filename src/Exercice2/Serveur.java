/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exercice2;

import Exercice2.Compute.ComputeEngine;
import Exercice2.Compute.Compute;
import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 *
 * @author agiavari
 */
public class Serveur {

    public static void main(String args []) {
        try {
            System.out.println("Serveur ouvert");
            LocateRegistry.createRegistry(1099);//Registre à l'adresse voulue
            
            //Nom qui sera enregistré dans le registre est utilisé par le client
            //Pour trouver l'objet
            String name = "rmi://"+InetAddress.getLocalHost().getHostAddress()+"/Compute";
            //On créait l'objet distant
            Compute engine = new ComputeEngine();
            Naming.rebind(name, engine);
            
        }catch(Exception e){System.out.println(e);}     
    }
}
